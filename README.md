# microbit-flowerpots-js

This is my plant automation project for [BBC micro:bit][microbit].

Currently, this repository holds a number of submodules with the actual
programs. This is because each BBC micro:bit involved is running one program,
but I want to be able to work with all of them from the same directory.

### Programs

The programs are written in static TypeScript and depends on
[Microsoft PXT][pxt] and [the micro:bit target for PXT][pxtmb]. See each
submodule for information on getting it up and running.

#### `moisture-monitor`

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/janlindblom/microbit-flowerpot-moisture-monitor-js.png)](https://bitbucket.org/janlindblom/microbit-flowerpot-moisture-monitor-js/)

This program gathers moisture levels from plants and distributes the levels to
all other devices on the same radio channel. Currently also acts as kind of a
master node at the moment.

#### `led-feedback`

[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/janlindblom/microbit-flowerpot-led-feedback-js.png)](https://bitbucket.org/janlindblom/microbit-flowerpot-led-feedback-js)

This program picks up moisture levels distributed over the radio and lights up
LEDs depending on the soil moisture: _yellow_ if it's starting to get dry and
_red_ when it's really dry.

[microbit]: https://microbit.org/
[pxt]: https://github.com/Microsoft/pxt
[pxtmb]: https://github.com/Microsoft/pxt-microbit
